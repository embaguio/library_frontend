var app = angular.module('app', ['ui.router', 'chart.js', 'ui.bootstrap', 'toaster', 'cp.ngConfirm']);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider, $uibModalProvider) {
    $httpProvider.defaults.headers.post = { 'Content-Type': 'application/x-www-form-urlencoded' };
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    }

    $urlRouterProvider.otherwise('/books');

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'src/views/login.html',
            controller: 'MainCtrl as vm',
            title: 'Login'
        })
        
        .state('books', {
            url: '/books',
            templateUrl: 'src/views/books.html',
            controller: 'BooksCtrl as vm',
            title: 'Books',
        })

        
        .state('borrow-management', {
            url: '/borrow-management',
            templateUrl: 'src/views/borrow_management.html',
            controller: 'BorrowCtrl as vm',
            title: 'Borrow Management',
            isAdmin: true,
        })

        
        .state('user-management', {
            url: '/user-management',
            templateUrl: 'src/views/user-management.html',
            controller: 'UserManagementCtrl as vm',
            title: 'User Management',
            isAdmin: true,
        })

});

app.run(function ($rootScope, $location, $state, mainService) {
    $rootScope.$watch(function(){
        return $state.$current.name
    }, function(){
        const auth = $state.$current.isAdmin;
        const role = mainService.getLoggedIn() && mainService.getLoggedIn().user_type_role;
        const loggedIn = mainService.isLoggedin();
        const onLoginPage = (loggedIn && $state.current.name === 'login');

        if (!loggedIn) {
            $state.go('login');
        } else if ( (auth && role !== 'Administrator') || onLoginPage ) {
            $state.go('books');
        }

        
    }) 
  });
