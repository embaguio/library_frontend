class BooksService {
       
    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }
        
    get() {
      return this.$http.post(`${this.apiBaseUrl}/book/get`)
          .then((response) => {
              return response && response.data;
          });
    }   
        
    getGenre() {
      return this.$http.post(`${this.apiBaseUrl}/book/get-genre`)
          .then((response) => {
              return response && response.data;
          });
    }   

    add(data) {
        return this.$http.post(`${this.apiBaseUrl}/book/add`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    update(data) {
        return this.$http.post(`${this.apiBaseUrl}/book/update`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    updateBookStatus(data) {
        return this.$http.post(`${this.apiBaseUrl}/book/update-status`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    delete(id) {
        return this.$http.post(`${this.apiBaseUrl}/book/delete`, id)
            .then((response) => {
                return response && response.data;
            });
    }
        
    getStudentBooks(data) {
      return this.$http.post(`${this.apiBaseUrl}/book/get-student-books`, data)
          .then((response) => {
              return response && response.data;
          });
    }
}
app.service('booksService', BooksService);