class UserService {
       
    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }
        
    get() {
      return this.$http.post(`${this.apiBaseUrl}/user/get`)
          .then((response) => {
              return response && response.data;
          });
    }   
        
    getUserTypes() {
      return this.$http.post(`${this.apiBaseUrl}/user/get-user-types`)
          .then((response) => {
              return response && response.data;
          });
    }   

    add(data) {
        return this.$http.post(`${this.apiBaseUrl}/user/add`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    update(data) {
        return this.$http.post(`${this.apiBaseUrl}/user/update`, data)
            .then((response) => {
                return response && response.data;
            });
    }   

    delete(id) {
        return this.$http.post(`${this.apiBaseUrl}/user/delete`, id)
            .then((response) => {
                return response && response.data;
            });
    }

    updatePassword(data) {
        return this.$http.post(`${this.apiBaseUrl}/user/update-password`, data)
            .then((response) => {
                return response && response.data;
            });
    }
}
app.service('userService', UserService);