class BorrowService {

    constructor($http, mainService) {
        this.$http = $http;
        this.mainService = mainService;
        this.apiBaseUrl = this.mainService.apiBaseUrl;
    }

    get() {
        return this.$http.post(`${this.apiBaseUrl}/borrow/get`)
            .then((response) => {
                return response && response.data;
            });
    }

    getLines(data) {
        return this.$http.post(`${this.apiBaseUrl}/borrow/get-lines`, data)
            .then((response) => {
                return response && response.data;
            });
    }

    saveLines(data) {
        console.log(data);
        return this.$http
            .post(`${this.apiBaseUrl}/borrow/update-lines`, data)
            .then(response => {
                return response && response.data;
            });
    }

    add(data) {
        return this.$http.post(`${this.apiBaseUrl}/borrow/add`, data)
            .then((response) => {
                return response && response.data;
            });
    }

    addLines(data) {
        return this.$http
            .post(`${this.apiBaseUrl}/borrow/add-lines`, data)
            .then(response => {
                return response && response.data;
            });
    }

    print(data) {
        return this.$http
            .post(`${this.apiBaseUrl}/borrow/print`, data)
            .then(response => {
                return response && response.data;
            });
    }
}
app.service('borrowService', BorrowService);