class BorrowCtrl {
    static injections() {
        return ['$scope', '$ngConfirm', '$window', 'toaster', 'modalService', 'borrowService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.get();
    }

    is_due(date) {
        return new Date() > new Date(date);
    }
    get() {
        this.borrowService.get()
            .then((response) => {
                if(!response) return;
                    this.borrows = response.data || [];
            });
    }

    updateBookStatus(data) {
        const is_active = data.book_is_active === '1' ? '0' : '1';
        const { book_id } = data;
        const params = { is_active,  book_id };
        const isActive = is_active === '1' ? true : false;
        
        if (isActive) {
            this.borrowService.updateBookStatus(params)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Book activated successfully!', 800);
                this.get();
            });
            return;
        }

        this.$ngConfirm({
            title: 'Deactivate this item?',
            content: 'Note: You can re-activate this item later on.',
            buttons: {
                deleteUser: {
                    text: 'Yes, deactivate',
                    btnClass: 'btn-red',
                    action: () => {
                        this.borrowService.updateBookStatus(params)
                        .then((response) => {
                            if(!response || !response.success) return;
                            this.toaster.pop('success', 'Success', 'Book deactivated successfully!', 800);
                            this.get();
                        });
                    }
                },
                cancel: function () {
                //    $ngConfirm('action is canceled');
                }
            }
        });
    }

    print(data) {
        this.borrowService.print(data).then(response => {
           this.$window.open(response.data, "_blank")
        });
    }
    
    return(data) {
        const modalSettings = {
            templateUrl: './src/views/modals/return_borrow.html',
            controller: 'ModalReturnCtrl',
            dataForComponent: { title: 'Return Books', data },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.get();
          }, () => {
            console.log('cancelled');
          });
    }
}

app.controller('BorrowCtrl', BorrowCtrl.ngConstruct());
