class BooksCtrl {
    static injections() {
        return ['$scope', '$ngConfirm','toaster', 'modalService', 'booksService', 'mainService'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.user = this.mainService.userInfo;
        this.get();
    }

    is_due(date) {
        return new Date() > new Date(date);
    }

    get() {
        if (this.user.user_type_id === '1') {
            this.booksService.get()
                .then((response) => {
                    if(!response) return;
                        this.books = response.data || [];
                });
        } else {
            this.booksService.getStudentBooks({ id: this.user.user_id })
                .then((response) => {
                    if(!response) return;
                        this.books = response.data || [];
                        console.log(this.books);
                });
        }
    }

    addBook() {
        const modalSettings = {
            templateUrl: './src/views/modals/add_book.html',
            controller: 'ModalBookCtrl',
            dataForComponent: { title: 'Add Book' },
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.get();
          }, () => {
            console.log('cancelled');
          });
    }

    updateBookStatus(data) {
        const is_active = data.book_is_active === '1' ? '0' : '1';
        const { book_id } = data;
        const params = { is_active,  book_id };
        const isActive = is_active === '1' ? true : false;
        
        if (isActive) {
            this.booksService.updateBookStatus(params)
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Book activated successfully!', 800);
                this.get();
            });
            return;
        }

        this.$ngConfirm({
            title: 'Deactivate this item?',
            content: 'Note: You can re-activate this item later on.',
            buttons: {
                deleteUser: {
                    text: 'Yes, deactivate',
                    btnClass: 'btn-red',
                    action: () => {
                        this.booksService.updateBookStatus(params)
                        .then((response) => {
                            if(!response || !response.success) return;
                            this.toaster.pop('success', 'Success', 'Book deactivated successfully!', 800);
                            this.get();
                        });
                    }
                },
                cancel: function () {
                //    $ngConfirm('action is canceled');
                }
            }
        });
    }

    editBook(book) {
        const modalSettings = {
            templateUrl: './src/views/modals/edit_book.html',
            controller: 'ModalBookCtrl',
            dataForComponent: { title: 'Edit Book', data: { ...book, book_pages:parseInt(book.book_pages) } }
          };

        this.modalService.open(modalSettings).result
          .then(() => {
            this.get();
          }, () => {
            console.log('cancelled');
          });
    }
}

app.controller('BooksCtrl', BooksCtrl.ngConstruct());
