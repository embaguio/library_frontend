class ModalBookCtrl {
    static injections() {
        return ['$uibModalInstance', 'props', 'booksService', 'mainService', 'toaster', 'props'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getGenre();
        if (this.props && this.props.data) {
            this.editData = this.props.data;
            this.editData = { ...this.editData, book_date_published: new Date(this.editData.book_date_published) };
        }
    }

    getGenre() {
        this.booksService.getGenre()
            .then((response) => {
                if(!response || !response.success) return;
                    this.genres = response.data;
            });
    }

    getStringDate(date) {
        return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
    }

    add(data) {
        const book_date_published = this.getStringDate(data.book_date_published);

        this.booksService.add( { ...data, book_date_published })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Book added successfully!', 800);
                this.close();
            });
    }

    edit(data) {
        const book_date_published = this.getStringDate(data.book_date_published);
        this.booksService.update({ ...data, book_date_published })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'Book updated successfully!', 800);
                this.close();
            });
    }

    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalBookCtrl', ModalBookCtrl.ngConstruct());
