class ModalUserCtrl {
    static injections() {
        return ['$uibModalInstance', 'props', 'userService', 'mainService', 'toaster', 'props'];
    }
    static ngConstruct() {
        return [...this.injections().map(el=>el.split(':').pop()), this];
    }
    constructor(...args) {
        this.constructor.injections().forEach((el,index) => this._defineKey(el,args[index]));
        this.setupController(args);
    }
    _defineKey(key, value) {
        Object.defineProperty(this, key.split(':')[0], {enumerable: true, value});
    }
    setupController(args) {
        this.init();
    }
    init() {
        this.getUserTypes();
        if (this.props && this.props.data) {
            this.userEditData = this.props.data;
            this.userEditData = { ...this.userEditData, user_birth_date: new Date(this.userEditData.user_birth_date) };
        }
    }

    getUserTypes() {
        this.userService.getUserTypes()
            .then((response) => {
                if(!response || !response.success) return;
                    this.userTypes = response.data;
                    console.log(this.userTypes);
            });
    }

    getStringDate(date) {
        return `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`;
    }

    addUser(data) {
        const user_birth_date = this.getStringDate(data.user_birth_date);

        this.userService.add( { ...data, user_birth_date })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'User added successfully!', 800);
                this.close();
            });
    }

    editUser(data) {
        const user_birth_date = this.getStringDate(data.user_birth_date);
        this.userService.update({ ...data, user_birth_date })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'User updated successfully!', 800);
                this.close();
            });
    }

    checkPassword(p1, p2) {
        this.isInvalidPassword = !(p1 === p2);
    }

    resetUserPassword(user_id, data) {

        this.userService.updatePassword({ ...data, user_id })
            .then((response) => {
                if(!response || !response.success) return;
                this.toaster.pop('success', 'Success', 'User password updated successfully!', 800);
                this.close();
            });
    }


    close() {
        this.$uibModalInstance.close();
    }
}

app.controller('ModalUserCtrl', ModalUserCtrl.ngConstruct());
