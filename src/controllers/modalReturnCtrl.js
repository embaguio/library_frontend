class ModalReturnCtrl {
  static injections() {
    return [
      "$uibModalInstance",
      "props",
      "booksService",
      "borrowService",
      "mainService",
      "toaster",
      "props"
    ];
  }
  static ngConstruct() {
    return [...this.injections().map(el => el.split(":").pop()), this];
  }
  constructor(...args) {
    this.constructor
      .injections()
      .forEach((el, index) => this._defineKey(el, args[index]));
    this.setupController(args);
  }
  _defineKey(key, value) {
    Object.defineProperty(this, key.split(":")[0], { enumerable: true, value });
  }
  setupController(args) {
    this.init();
  }

  init() {
    this.form = this.props.data;
    console.log(this.form);
    this.form = { ...this.form, 
      borrow_date: new Date(this.form.borrow_date),
      borrow_due_date: new Date(this.form.borrow_due_date),
    };
    this.getLines(this.form);
  }

  getLines(data) {
    this.borrowService.getLines(data)
    .then((response) => {
        if(!response) return;
            this.form = { ...this.form, borrow_lines: response.data };
    });
  }


  getStringDate(date) {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }

  save(borrow, data) {
    this.borrowService.saveLines({ ...data, borrow } )
      .then(response => {
        if (!response || !response.success) return;
        this.toaster.pop("success", "Success","Transaction was successful!", 800);
        this.close();
      });
  }

  close() {
    this.$uibModalInstance.close();
  }
}

app.controller("ModalReturnCtrl", ModalReturnCtrl.ngConstruct());
