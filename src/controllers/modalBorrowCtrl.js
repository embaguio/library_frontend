class ModalBorrowCtrl {
  static injections() {
    return [
      "$uibModalInstance",
      "props",
      "booksService",
      "borrowService",
      "mainService",
      "toaster",
      "props"
    ];
  }
  static ngConstruct() {
    return [...this.injections().map(el => el.split(":").pop()), this];
  }
  constructor(...args) {
    this.constructor
      .injections()
      .forEach((el, index) => this._defineKey(el, args[index]));
    this.setupController(args);
  }
  _defineKey(key, value) {
    Object.defineProperty(this, key.split(":")[0], { enumerable: true, value });
  }
  setupController(args) {
    this.init();
  }

  init() {
    this.form = { ...this.form, borrow_lines: [], borrow_date: new Date() };
    this.getGenre();
    this.getBooks();
    if (this.props && this.props.data) {
      this.editData = this.props.data;
      this.editData = {
        ...this.editData,
        book_date_published: new Date(this.editData.book_date_published)
      };
    }
  }

  selectBook(book, borrow_line) {
    borrow_line.selectedBook = angular.copy(book);
    borrow_line.hasSelectedBook = true;
  }

  getBooks() {
    this.booksService.get().then(response => {
      if (!response) return;
      this.books = response.data || [];
    });
  }

  addNew() {
    if (this.isEdit) {
      this.editData.form.borrow_lines.push({
        id: this.form.borrow_lines.length + 1
      });
    } else {
      this.form.borrow_lines.push({
        id: this.form.borrow_lines.length + 1
      });
    }
  }

  remove(index) {
    if (this.isEdit) {
      this.editData.form.borrow_lines.splice(index, 1);
    } else {
      this.form.borrow_lines.splice(index, 1);
    }
  }

  getGenre() {
    this.booksService.getGenre().then(response => {
      if (!response || !response.success) return;
      this.genres = response.data;
    });
  }

  getStringDate(date) {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }

  add(data) {
    const user_id = this.editData.user_id;
    const borrow_date = this.getStringDate(data.borrow_date);
    const borrow_due_date = this.getStringDate(data.borrow_due_date);
    const total_books_borrowed = this.getTotalBorrowed(data && data.borrow_lines);

    const params = { borrow_date, borrow_due_date, user_id, total_books_borrowed };
    this.borrowService.add(params)
        .then((response) => {
            if(!response || !response.success) return;
            this.addLines(data.borrow_lines, response.data);
        });
  }

  searchData(data){
    return (item) => {
      if (data === undefined) return true;
        if ((item.book_name.toLowerCase().indexOf(data)!=-1 || item.book_isbn.indexOf(data)!=-1) && item.book_is_active === '1') {
          return true;
      }
      return false;
    }
  };

  addLines(data, borrow_id) {
      
    let borrowLines = [];
    data.forEach((borrow) => {
        const { borrow_line_qty } = borrow;
        const { book_id } = borrow.selectedBook;
        borrowLines = [ ...borrowLines, { book_id, borrow_line_qty }];
    });


    const params = { borrow_id, ...borrowLines };

    this.borrowService.addLines(params)
        .then(response => {
            if (!response || !response.success) return;
            this.toaster.pop("success", "Success", "Created successfully!", 800);
            this.close();
        });
  }

  getTotalBorrowed(data) {
    let total = 0;

    data.forEach(line => {
        total += parseFloat(line.borrow_line_qty) || 0;
      });

    return total || 0;
  }

  edit(data) {
    const book_date_published = this.getStringDate(data.book_date_published);
    this.booksService
      .update({ ...data, book_date_published })
      .then(response => {
        if (!response || !response.success) return;
        this.toaster.pop(
          "success",
          "Success",
          "Book updated successfully!",
          800
        );
        this.close();
      });
  }

  close() {
    this.$uibModalInstance.close();
  }
}

app.controller("ModalBorrowCtrl", ModalBorrowCtrl.ngConstruct());
